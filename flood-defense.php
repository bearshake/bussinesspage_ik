<?php
/**
 * Created by PhpStorm.
 * User: BearShake
 * Date: 28.03.2018
 * Time: 23:42
 */



session_start();
$time=microtime(1);
if (!isset($_SESSION["arr_time"])) $_SESSION["arr_time"]=array(0,0,0);

$min_time=min($_SESSION["arr_time"]);
if ($time-$min_time < 3.5) die("Anti-flood");

$min_index=array_search($min_time,$_SESSION["arr_time"]);
$_SESSION["arr_time"][$min_index]=$time;
?>