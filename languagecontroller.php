<?php
/**
 * Created by PhpStorm.
 * User: BearShake
 * Date: 28.03.2018
 * Time: 21:18
 */

include 'SimpleQueryExecute.php';


class languagecontroller {



	function __construct($lang) {
		switch ($lang)
		{
			case 'ru':
				$this->lg = 'ru';
				break;

			case 'lv':
				$this->lg = 'lv';
				break;
		}
	}

	function printTitle()
	{
		if($this->lg == 'ru')
			echo "Ведение уголовных дел (защита на стадии дознания, следствия, судебная защита)";
		else echo "Krimināllietu(aizsardzība stadijā дознания, izmeklēšanas, tiesu aizsardzība))";
	}

	function pritnLangs()
	{
		if($this->lg == 'lv'){
			echo    '<li id="ru">
						<a href="#ru" onclick="langControl()">
							<img src="img/rus-icon.png" width="30" height="20"/>
							Krieviski
						</a>
					</li>';

		}else{
			echo    '<li id="lv">
						<a href="#en" onclick="langControl()">
							<img src="img/latvija.png" width="30" height="20"/>
							Latviski
						</a>
					</li>
					';
		}

	}

	function fTitle()
	{
		if($this->lg == 'lv') echo '<h2>Darba</h2>
									<p>Sfēras</p>';
		else echo '<h2>Сферы</h2>
					<p>Занятости</p>';
	}

	function printFeatures()
	{
		if($this->lg == 'lv') echo '<div class="col-sm-8 bg_yel">
				<div class="row slideanim">
					<div class="col-sm-6">
						<div class="row itemList">
							<div class="col-sm-2 text-right">
								<div class="glyphicon glyphicon-tag"></div>
							</div>
							<div class="col-sm-10">
								<h5><strong>Konsultešana</strong></h5>
							</div>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					</div>
					<div class="col-sm-6">
						<div class="row itemList">
							<div class="col-sm-2 text-right">
								<div class="glyphicon glyphicon-tag"></div>
							</div>
							<div class="col-sm-10">
								<h5><strong>Juridiska palīdzība</strong></h5>
							</div>
						</div>
                        <div class="row myalign-right">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis enim excepturi ipsum nobis ratione.</p>
                        </div>

					</div>
				</div>
				<div class="row slideanim">
					<div class="col-sm-6">
						<div class="row itemList">
							<div class="col-sm-2 text-right">
								<div class="glyphicon glyphicon-tag"></div>
							</div>
							<div class="col-sm-10">
								<h5><strong>Gramātvežu darbība</strong></h5>
							</div>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est.</p>
					</div>
					<div class="col-sm-6">
						<div class="row itemList">
							<div class="col-sm-2 text-right">
								<div class="glyphicon glyphicon-tag"></div>
							</div>
							<div class="col-sm-10">
								<h5><strong>Ekonomiska apkalpošana</strong></h5>
							</div>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					</div>
				</div>
				<div class="row slideanim">
					<div class="col-sm-6">
						<div class="row itemList">
							<div class="col-sm-2 text-right">
								<div class="glyphicon glyphicon-tag"></div>
							</div>
							<div class="col-sm-10">
								<h5><strong>Motivēsāna</strong></h5>
							</div>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum dolor doloribus dolorum et, laudantium sequi.</p>
					</div>
					<div class="col-sm-6">
						<div class="row itemList">
							<div class="col-sm-2 text-right">
								<div class="glyphicon glyphicon-tag"></div>
							</div>
							<div class="col-sm-10">
								<h5><strong>Kursi</strong></h5>
							</div>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium asperiores fugiat odit?</p>
					</div>
				</div>
			</div>';

		//ELSE

		else echo '<div class="col-sm-8 bg_yel">
				<div class="row slideanim">
					<div class="col-sm-6">
						<div class="row itemList">
							<div class="col-sm-2 text-right">
								<div class="glyphicon glyphicon-tag"></div>
							</div>
							<div class="col-sm-10">
								<h5><strong>Консультирование</strong></h5>
							</div>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					</div>
					<div class="col-sm-6">
						<div class="row itemList">
							<div class="col-sm-2 text-right">
								<div class="glyphicon glyphicon-tag"></div>
							</div>
							<div class="col-sm-10">
								<h5><strong>Помощь юристов</strong></h5>
							</div>
						</div>
                        <div class="row myalign-right">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis enim excepturi ipsum nobis ratione.</p>
                        </div>

					</div>
				</div>
				<div class="row slideanim">
					<div class="col-sm-6">
						<div class="row itemList">
							<div class="col-sm-2 text-right">
								<div class="glyphicon glyphicon-tag"></div>
							</div>
							<div class="col-sm-10">
								<h5><strong>Бухгалтерская деятельность</strong></h5>
							</div>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est.</p>
					</div>
					<div class="col-sm-6">
						<div class="row itemList">
							<div class="col-sm-2 text-right">
								<div class="glyphicon glyphicon-tag"></div>
							</div>
							<div class="col-sm-10">
								<h5><strong>Экономическая деятельность</strong></h5>
							</div>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					</div>
				</div>
				<div class="row slideanim">
					<div class="col-sm-6">
						<div class="row itemList">
							<div class="col-sm-2 text-right">
								<div class="glyphicon glyphicon-tag"></div>
							</div>
							<div class="col-sm-10">
								<h5><strong>Мотивация</strong></h5>
							</div>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum dolor doloribus dolorum et, laudantium sequi.</p>
					</div>
					<div class="col-sm-6">
						<div class="row itemList">
							<div class="col-sm-2 text-right">
								<div class="glyphicon glyphicon-tag"></div>
							</div>
							<div class="col-sm-10">
								<h5><strong>Курсы</strong></h5>
							</div>
						</div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium asperiores fugiat odit?</p>
					</div>
				</div>
			</div>';
	}

	function printAbout()
	{
		if($this->lg == 'lv') echo '<h1 class="text-center">Par mūms</h1>
			<p>
				Jūs varat pārvietot viņu jebkādā vietā uz lapaspuses. Izstāstiet saita apmeklētājiem par sevi. Še veiksmīgi paskatīsies teksts par jūsu kompāniju un pakalpojumiem.
			</p>
			<p>
				Izstāstiet interesantu stāstu, piemēram, kā jums galvā atnāca personīgās lietas ideja, un izskaidrosiet, kur noslēdzas jūsu priekšrocība pirms konkurentiem. Atvediet aizraujošos faktus un ciparus - lai jūs iegaumēs. Ne aizmirstiet par atslēgu vārdiem, pa kuriem jūsu saitu atradīs meklējumu sistēmās.
			</p>';
		else echo '<h1 class="text-center">О нас</h1>
			<p>
				Вы можете переместить его в любое место на странице. Расскажите посетителям сайта о себе. Здесь будет удачно смотреться текст о вашей компании и услугах.​
			</p>
			<p>
				Расскажите интересную историю, например, как вам в голову пришла идея собственного дела, и объясните, в чем заключается ваше преимущество перед конкурентами. Приводите увлекательные факты и цифры — пусть вас запомнят. Не забудьте про ключевые слова, по которым ваш сайт найдут в поисковых системах.
			</p>';

	}

	function printGuaranties()
	{
		if($this->lg == 'lv') echo '<h2>Es garāntēju</h2>
	<div class="row adv-row">
		<div class="col-sm-4">
			<span class="glyphicon glyphicon-certificate logo-small"></span>
			<h4>Sertificeto palidzību</h4>
			<p>Sertificeto palidzību</p>
		</div>
		<div class="col-sm-4">
			<span class="glyphicon glyphicon glyphicon-lock logo-small"></span>
			<h4>Konfidencialitati</h4>
			<p>Lorem ipsum dolor sit amet.</p>
		</div>
		<div class="col-sm-4">
			<span class="glyphicon glyphicon-off logo-small"></span>
			<h4>Precizitati</h4>
			<p>Lorem ipsum dolor sit.</p>
		</div>
	</div>
	<div class="row adv-row">
		<div class="col-sm-4">
			<span class="glyphicon glyphicon glyphicon-hourglass logo-small"></span>
			<h4>Planu</h4>
			<p>Lorem ipsum.</p>
		</div>
		<div class="col-sm-4">
			<span class="glyphicon glyphicon-pushpin logo-small"></span>
			<h4>Uzdevumi un merķi</h4>
			<p>Sertificeto palidzību</p>
		</div>
		<div class="col-sm-4">
			<span class="glyphicon glyphicon-thumbs-up logo-small"></span>
			<h4>Palīdzība un atbalsts</h4>
			<p>Sertificeto palidzību</p>
		</div>
	</div>';
		else echo '<h2>Я гарантирую</h2>
	<div class="row adv-row">
		<div class="col-sm-4">
			<span class="glyphicon glyphicon-certificate logo-small"></span>
			<h4>Сертифицированность</h4>
			<p>Sertificeto palidzību</p>
		</div>
		<div class="col-sm-4">
			<span class="glyphicon glyphicon glyphicon-lock logo-small"></span>
			<h4>Конфиденциальность</h4>
			<p>Sertificeto palidzību</p>
		</div>
		<div class="col-sm-4">
			<span class="glyphicon glyphicon-off logo-small"></span>
			<h4>Конкретность</h4>
			<p>Sertificeto palidzību</p>
		</div>
	</div>
	<div class="row adv-row">
		<div class="col-sm-4">
			<span class="glyphicon glyphicon glyphicon-hourglass logo-small"></span>
			<h4>Планировка</h4>
			<p>Sertificeto palidzību</p>
		</div>
		<div class="col-sm-4">
			<span class="glyphicon glyphicon-pushpin logo-small"></span>
			<h4>Цели и задачи</h4>
			<p>Sertificeto palidzību</p>
		</div>
		<div class="col-sm-4">
			<span class="glyphicon glyphicon-thumbs-up logo-small"></span>
			<h4>Помощь и поддержка</h4>
			<p>Sertificeto palidzību</p>
		</div>
	</div>';
	}

	function printReviews()
	{
		if($this->lg == 'lv') echo '<div class="carousel-inner" role="listbox">
		<div class="item active">
			<h4 class="ats"><strong>"Šī kompānija ir vislabaka. Es esmu tā laimīgs ar rezultātu!"</strong><br><span>Michael Roe, Vice President, Comment Box</span></h4>
		</div>
		<div class="item">
			<h4 class="ats"><strong>"Viena vardā VAU!!"</strong><br><span>John Doe, Salesman, Rep Inc</span></h4>
		</div>
		<div class="item">
			<h4 class="ats"><strong>"Vēl laimigāk būt nav iespejams"</strong><br><span>Chandler Bing, Actor, FriendsAlot</span></h4>
		</div>
	</div>';
		//ELSE
		else echo '<div class="carousel-inner" role="listbox">
		<div class="item active">
			<h4 class="ats"><strong>"Отличное обслуживание, я доволен результатом"</strong><br><span>Michael Roe, Vice President, Comment Box</span></h4>
		</div>
		<div class="item">
			<h4 class="ats"><strong>"Одним словом ВАУ"</strong><br><span>John Doe, Salesman, Rep Inc</span></h4>
		</div>
		<div class="item">
			<h4 class="ats"><strong>"Лучше просто быть не МОЖЕТ! КЛАСС"</strong><br><span>Chandler Bing, Actor, FriendsAlot</span></h4>
		</div>
	</div>';

	}

	function printContact()
	{
		if($this->lg == 'lv') echo '<h1><strong>SAZINIETIES</strong> AR MŪMS</h1>';
		else echo '<h1><strong>СВЯЖИТЕСЬ</strong> С НАМИ</h1>';
	}
}

/*<?php
				$coks = $_COOKIE['lang'];
				switch ($coks)
				{
					case 'ru':
						echo "<h2 class=\"handled\"><strong>Ирина</strong> Кузменкова</h2>";
						break;
					case 'lv':
						echo "<h2 class=\"handled\"><strong>Irīna</strong> Kuzmenkova</h2>";
						break;
				}
				?>
*/