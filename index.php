
<?php
error_reporting(E_ALL);
require_once 'flood-defense.php';
include 'languagecontroller.php';
$lang='ru';
if(isset($_COOKIE['lang'])){
	$lang = $_COOKIE['lang'];
}else{
    setcookie('lang','ru',2);
    $lang = 'ru'; //default
}
$selector = new languagecontroller($lang);
?>


<!doctype html>
<html class="no-js" lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="manifest" href="site.webmanifest">
	<link rel="apple-touch-icon" href="icon.png">
	<!-- Place favicon.ico in the root directory -->

	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/custom.css">
</head>


<body>
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNav">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#myPage">Home</a>
		</div>
		<div class="collapse navbar-collapse" id="myNav">
			<ul class="languagepicker roundborders">
				<?php $selector->pritnLangs()?>

			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#about">PAR MŪMS</a></li>
				<li><a href="#services">PREKŠROCĪBAS</a></li>
				<li><a href="#portfolio">PORTFOLIO</a></li>
				<li><a href="#myCarousel">ATSAUKSMES</a></li>
				<li><a href="#contact">KONTAKTI</a></li>
			</ul>
		</div>
	</div>
</nav>

<div class="container-fluid main-info">
	<div class="col-sm-6 aac">
		<div class="row">
			<div class="col-sm-5">
			</div>
			<div class="col-sm-7">
                <h2 class="handled">Irina Kuzmenkova</h2>
				<p class="handled"><?php $selector->printTitle() ?></p>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid fact">
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-4" >
				<div class="row centered">
					<div class="col-sm-6"></div>
					<div class="col-sm-6">
						<div class="row">
							<div class="col-sm-6">
								<?php $selector->fTitle() ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php $selector->printFeatures() ?>
		</div>
	</div>

</div>

<div class="container-fluid blue" id="about">
	<div class="row info">
		<div class="col-sm-12">
			<?php $selector->printAbout() ?>
		</div>
        <a href="" id="services"></a>
	</div>
</div>

<!-- Services -->

<div class="container text-center contact">
	<?php $selector->printGuaranties() ?>
</div>

<div class="container text-center contact" id="portfolio">
	<h2 class="text-center">Portfolio</h2>
	<div class="row">
		<div class="col-sm-4">
			<div class="portfolio-block">
				<img src="img/portfolio-images/portf_1.jpg" alt="portf1" width="100%" height="100%">
			</div>
		</div>
		<div class="col-sm-4">
			<div class="portfolio-block">
				<img src="img/portfolio-images/portf_2.jpg" alt="portf2" width="100%" height="100%">
			</div>
		</div>
		<div class="col-sm-4">
			<div class="portfolio-block">
				<img src="img/portfolio-images/portf_3.jpg" alt="portf3" width="100%" height="100%">
			</div>
		</div>
	</div>
</div>

<!-- Gallery/Gallery -->
<div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
		<li data-target="#myCarousel" data-slide-to="2"></li>
	</ol>
	<!-- Wrapper for slides -->
	<?php $selector->printReviews() ?>
</div>

<!-- Contact -->
<div class="container-fluid contact2" id="contact">
	<div class="row c-info">
		<div class="col-sm-5">
			<div class="row">
				<div class="col-sm-12">
					<div class="row cnt">
						<div class="col-sm-12">
                            <?php $selector->printContact() ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-7 right-col">
			<div class="row">
				<div class="col-sm-6">
					<div class="row controlled">
						<h3 class="text-center">Mūsu adrese</h3>
						<div class="col-sm-12">
							<ul>
								<li><h5>Ул. Резнас 10ц</h5></li>
								<li><h5>Email: anton@inbox.lv</h5></li>
								<li><h5>Телефон:  +371 26887471</h5></li>
							</ul>
						</div>
					</div>
				</div>
                <div class="col-sm-6">
                    <div class="row comb">
                        <h3 class="text-center">Примечание:</h3>
                        <ul class="text-center">
                            <li>Lorem ipsum dolor sit amet.</li>
                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                            <li>Lorem ipsum dolor.</li>
                            <li>Lorem ipsum dolor sit.</li>
                            <li>Lorem ipsum dolor sit.</li>
                        </ul>
                    </div>
                </div>
			</div>
            <h3>Заполните форму!</h3>
			<div class="row">
				<div class="col-sm-6">
                    <form method="post" action="mail.php">
                        <input type="text" name="Name" class="form-control" placeholder="Vards">
                        <input type="email" name="Email" class="form-control" placeholder="Email" >
                        <input type="text" name="Theme" class="form-control" placeholder="Tēma">
                        <textarea class="form-control" name="comment" id="comment" cols="30" rows="10" placeholder="Komentārs"></textarea>
                        <button class="btn btn-default pull-right" type="submit">Send</button>
                    </form>
					</div>
			</div>
		</div>
	</div>
</div>


<!-- Footer -->
<div class="container-fluid footer">
	<div class="row">
		<div class="col-sm-6">
			<br><script type="text/javascript"> document.write(new Date().getFullYear()); </script>
            ANTONS POCELUJONOKS (C)
		</div>
	</div>
</div>


<script src="js/vendor/modernizr-3.5.0.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
    window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
    ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
</script>
<script src="https://www.google-analytics.com/analytics.js" async defer></script>

<script>
    $(document).ready(function(){
        $(".handled").each(function () {
            $(this).addClass("show");
        });
        // Add smooth scrolling to all links in navbar + footer link
        $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();
                // Store hash
                var hash = this.hash;
                 $('html, body').animate({
                    scrollTop:  $(hash).offset().top
                } , 900, function(){
                     window.location.hash = hash;
                });
            } // End if
        });

        $(window).scroll(function() {
            $(".slideanim").each(function(){
                var pos = $(this).offset().top;

                var winTop = $(window).scrollTop();
                if (pos < winTop + 600) {
                    $(this).addClass("slide");
                }
            });
        });
    })
</script>


<script>
    function langControl() {
        var lang = getCookie('lang');
        if(lang == ''){
            alert("Cookie is empty, setting ru lang");
            document.cookie = "lang=ru";
        }else{
            switch (getCookie('lang')){
                case 'lv':
                    document.cookie = "lang=ru";
                    location.reload();
                    break;
                case 'ru':
                    document.cookie = "lang=lv";
                    location.reload();
                    break;
            }
        }
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
</script>


</body>
</html>
